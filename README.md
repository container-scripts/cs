ContainerScripts
================

## SYNOPSIS

   `cs [-x] [@<container>] <command> [<arg>...]`


## DESCRIPTION

   ContainerScripts is a shell script framework for containers.

   Each container is like a virtual machine that has an application
   installed inside. Each container has a base directory where the
   settings of the container are stored (in the file
   `settings.sh`). The command `cs` picks the parameters that it needs
   from the file `settings.sh` in the container's directory.

   Normally the commands are issued from inside the container's
   directory, however the option `@<container>` can be used to specify
   the context of the command.

   The option `-x` can be used for debugging.

   For more details see the man page and the wiki pages:

   - https://container-scripts.gitlab.io/cs/man/
   - https://gitlab.com/container-scripts/cs/wiki


## INSTALLATION

First install `podman`:

    source /etc/os-release
    echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/ /" | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
    curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/Release.key | sudo apt-key add -
    apt update
    apt install --yes dnsmasq
    apt install --yes podman

Then install `cs`:

    apt install git make m4
    git clone https://gitlab.com/container-scripts/cs /opt/container-scripts/cs
    cd /opt/container-scripts/cs/
    make install

**Note:** Starting `dnsmasq` may fail because it needs to use port
`53`, which is often used by `systemd-resolved.service` (you can check
this with `lsof -i :53`). You can disable `systemd-resolved` like this:

    systemctl stop systemd-resolved
    systemctl disable systemd-resolved
    systemctl mask systemd-resolved

Now you need to fix resolving DNS queries:

    ls -l /etc/resolv.conf
    rm /etc/resolv.conf
    cat <<EOF > /etc/resolv.conf
    nameserver 8.8.8.8
    nameserver 9.9.9.9
    EOF

Finally, enable and start `dnsmasq`:

    systemctl enable dnsmasq
    systemctl start dnsmasq

Without `dnsmasq` running, you will fail to access the internet from
the containers.

## EXAMPLES

### Installing Web Server Proxy

    cs pull wsproxy
    cs init wsproxy @wsproxy
    cd /var/cs/wsproxy/
    vim settings.sh
    cs make


### Installing Moodle

    cs pull moodle
    cs init moodle @moodle.example.org
    cd /var/cs/moodle.example.org/
    vim settings.sh
    cs make


### Installing ShellInABox

    cs pull shellinabox
    cs init shellinabox @shell1
    source cs cd @shell1
    vim settings.sh
    cs make


## COMMANDS

* `pull <app> [<branch>]`

    Clone or pull `https://gitlab.com/container-scripts/<app>` to
    `/opt/container-scripts/<app>`. A certain branch can be specified
    as well. When a branch is given, then it is saved to
    `/opt/container-scripts/<app>-<branch>`.

* `init <app> [@<container>]`

    Initialize a container directory by getting the file `settings.sh`
    from the given app directory. If the second argument is missing,
    the current directory will be used to initialize the container,
    otherwise it will be done on `/var/cs/<container>`.

* `info`

    Show some info about the container of the current directory.

* `build`, `create`, `config`

    Build the image, create the container, and configure the guest
    system inside the container.

* `inject <script>`

    Inject and run a script inside the container.

* `start`, `stop`, `restart`

    Start, stop and restart the container.

* `shell`

    Get a shell on the container.

* `exec`

    Execute a command from outside the container.

* `snapshot`

    Make a snapshot of the container.

* `remove`

    Remove the container and the image.

* `runtest [-d|--debug] [<test-script.t.sh>...]`

    Run the given test scripts. If no test-script is given all the
    test scripts in the working directory will be run.  Test scripts
    have the extension `.t.sh`

* `test [-d|--debug] [<test-script.t.sh>...]`

    Run the given test scripts inside the cs-test container.  It
    actually call the command `runtest` inside the container with the
    same options and arguments.

* `help`

    Display a help message.


