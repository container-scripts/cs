description 'Test cs help'

test_case 'cs help' '
    cs help 2>&1 | grep "No file ./settings.sh found."
'

test_case 'cs @test2 help' '
    cs pull cs &&
    rm -rf $CONTAINERS/test2 &&
    cs init cs/test/app2 @test2 &&
    cs @test2 help | grep "DockerScripts is a shell script framework for Docker."
'

test_case 'cs help' '
    cd $CONTAINERS/test2 &&
    local helpmsg="$(cs help)" &&
    echo "$helpmsg" | grep "DockerScripts is a shell script framework for Docker." &&
    echo "$helpmsg" | grep "apache2"
'

test_case 'cs -x help' '
    cd $CONTAINERS/test2 &&
    [[ "$(cs -x help 2>&1 | grep + | wc -l)" -gt 100 ]] &&
    [[ "$(cs -x @test2 help 2>&1 | grep + | wc -l)" -gt 30 ]] &&
    [[ "$(cs @test2 -x help 2>&1 | grep + | wc -l)" -gt 100 ]]
'
