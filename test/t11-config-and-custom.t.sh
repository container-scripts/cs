description 'Test configuration, overriding commands, adding new commands, etc.'

test_case 'cs @test2 create' '
    cs pull cs &&
    rm -rf $CONTAINERS/test2 &&
    cs init cs/test/app2 @test2
    cs @test2 build &&
    cs @test2 create
'

test_case 'cs @test2 start,stop,restart (overriden commands)' '
    cs @test2 stop &&
    cs @test2 start | grep "Starting container" &&
    cs @test2 stop | grep "Stoping container" &&
    cs @test2 restart | grep "Restarting container"
'

test_case 'cs @test2 config (run configuration scripts)' '
    cs @test2 config | grep "Running script"
'

test_case 'cs @test2 inject test1.sh (config script "test1")' '
    cs @test2 exec rm -f /etc/test1.sh &&
    cs @test2 inject test1.sh &&
    cs @test2 exec cat /etc/test1.sh | grep CONTAINER
'

test_case 'cs @test2 inject site.sh [enable|disable] (config script "site")' '
    cs @test2 inject site.sh | grep "Usage" &&
    cs @test2 inject site.sh disable &&
    cs @test2 exec test ! -f /etc/apache2/sites-enabled/default.conf &&
    cs @test2 inject site.sh enable &&
    cs @test2 exec test -f /etc/apache2/sites-enabled/default.conf
'

test_case 'cs @test2 apache2 [start|stop] (new command apache2)' '
    cs @test2 apache2 start &&
    [[ $(cs @test2 exec ps ax | grep -v grep | grep apache2 -c) > 0 ]] &&
    cs @test2 apache2 stop &&
    [[ $(cs @test2 exec ps ax | grep -v grep | grep apache2 -c) == 0 ]] &&
    cs @test2 apache2 start &&
    [[ $(cs @test2 exec ps ax | grep -v grep | grep apache2 -c) > 0 ]]
'

test_case 'cs @test1 create' '
    cs pull cs &&
    rm -rf $CONTAINERS/test1 &&
    cs init cs/test/app1 @test1
    cs @test1 build &&
    cs @test1 create
'

test_case 'cs @test1 test2 [start|stop] (new global command test2)' '
    cs @test1 help | grep "test2" &&
    cs @test1 test2 start &&
    [[ $(cs @test2 exec ps ax | grep -v grep | grep apache2 -c) > 0 ]] &&
    cs @test1 test2 stop &&
    [[ $(cs @test2 exec ps ax | grep -v grep | grep apache2 -c) == 0 ]] &&
    cs @test1 test2 start &&
    [[ $(cs @test2 exec ps ax | grep -v grep | grep apache2 -c) > 0 ]]
'

