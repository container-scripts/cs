description 'Test remove'

cs pull cs
cs @test1 stop
rm -rf $CONTAINERS/test1
cs @test2 stop
rm -rf $CONTAINERS/test2

test_case 'cs @test1 create' '
    cs init cs/test/app1 @test1 &&
    cs @test1 build &&
    cs @test1 create &&
    cs @test1 start
'

test_case 'cs @test2 create' '
    cs init cs/test/app2 @test2 &&
    cs @test2 build &&
    cs @test2 create &&
    cs @test2 start
'

test_case 'cs @test1 remove' '
    cs @test1 remove &&
    [[ -z "$(podman ps -a --format "{{.Names}}" | grep cs-test-app1)" ]] &&
    [[ -z "$(podman images --format "{{.Repository}}" | grep cs-test-app1)" ]]
'

test_case 'cs @test2 remove' '
    cs @test2 remove &&
    test ! -f $CSDIR/cmd/test2.sh
'

# cleanup
#rm -rf "$HOME"
