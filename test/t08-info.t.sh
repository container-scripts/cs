description 'Test info and snapshot'

test_case 'cs create' '
    cs pull cs &&
    rm -rf $CONTAINERS/test1 &&
    cs init cs/test/app1 @test1
    cs @test1 build &&
    cs @test1 create &&
    cs @test1 start &&
    podman ps --format "{{.Names}}" | grep cs-test-app1
'

test_case 'cs @test1 info' '
    local output="$(cs @test1 info)" &&
    echo "$output" | grep SETTINGS &&
    echo "$output" | grep IMAGE &&
    echo "$output" | grep CONTAINER
'

test_case 'cs @test1 snapshot' '
    local snapshot="cs-test-app1:$(date +%F)" &&
    cs @test1 snapshot | grep "Making a snapshot of cs-test-app1 to $snapshot" &&
    cs @test1 info | grep $snapshot &&
    podman rmi $snapshot
'
