description 'Test cs start'

test_case 'cs create' '
    cs pull cs &&
    rm -rf $CONTAINERS/test1 &&
    cs init cs/test/app1 @test1
    cs @test1 build &&
    cs @test1 create &&
    podman ps -a --format "{{.Names}}" | grep cs-test-app1
'

test_case 'cs @test1 start' '
    cs @test1 start &&
    podman ps --format "{{.Names}}" | grep cs-test-app1
'

test_case 'cs @test1 stop' '
    cs @test1 stop &&
    [[ -z $(podman ps --format "{{.Names}}" | grep cs-test-app1) ]]
'

test_case 'cs start' '
    cd $CONTAINERS/test1 &&
    cs start &&
    podman ps --format "{{.Names}}" | grep cs-test-app1
'

test_case 'cs stop' '
    cs stop &&
    [[ -z $(podman ps --format "{{.Names}}" | grep cs-test-app1) ]]
'

test_case 'cs restart' '
    cs restart &&
    podman ps --format "{{.Names}}" | grep cs-test-app1
'
