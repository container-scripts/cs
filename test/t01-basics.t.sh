description 'Basic checks'

test_case 'cs' '
    [[ "$(cs | grep APPS)" == "APPS=$APPS" ]]
'

test_case 'cs -v ; cs --version' '
    cs -v | grep DockerScript &&
    cs --version | grep DockerScript
'

test_case 'cs -h ; cs --help' '
    cs -h | grep "The commands are listed below" &&
    cs --help | grep "The commands are listed below"
'

