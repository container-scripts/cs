description 'Test custom dirs'

test_case 'cs init custom_dir/cs/test/app1 @test1' '
    rm -rf $CONTAINERS/test1 &&
    rm -rf $HOME/cs &&
    git clone https://gitlab.com/container-scripts/cs $HOME/cs &&
    cs init $HOME/cs/test/app1 @test1 &&
    cs @test1 build &&
    cs @test1 create &&
    cs @test1 start
'

test_case 'cs init cs/test/app1 @custom_dir' '
    rm -rf $HOME/test1 &&
    cs pull cs &&
    cs init cs/test/app1 @$HOME/test1 &&
    cs @$HOME/test1 build &&
    cd $HOME/test1 &&
    cs create &&
    cs start
'
