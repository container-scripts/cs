description 'Test cs pull'

test_case 'cs pull (cloning)' '
    rm -rf $APPS/cs &&
    cs pull cs 2>&1 | grep "Cloning into" &&
    [[ -d $APPS/cs ]]
'

test_case 'cs pull (up to date)' '
    cs pull cs 2>&1 | grep "Already up to date."
'

test_case 'cs pull (with branch)' '
    cs pull cs gh-pages &&
    [[ -d $APPS/cs-gh-pages ]] &&
    [[ $(git -C $APPS/cs-gh-pages branch) == "* gh-pages" ]] &&
    rm -rf $APPS/cs-gh-pages
'

test_case 'cs pull dummy (not found)' '
    cs pull dummy 2>&1 | grep "not found"
'

test_case 'cs pull (usage)' '
    cs pull 2>&1 | grep "Usage:"
'
