description 'Test cs init'

test_case 'cs init cs/test/app1 @test1' '
    rm -rf $CONTAINERS/test1 &&
    cs pull cs &&
    cs init cs/test/app1 @test1 &&
    [[ -d $CONTAINERS/test1 ]] &&
    [[ -f $CONTAINERS/test1/settings.sh ]]
'

test_case 'cs init cs/test/app1' '
    rm -rf $CONTAINERS/test1 &&
    mkdir -p  $CONTAINERS/test1 &&
    cd $CONTAINERS/test1 &&
    cs init cs/test/app1 &&
    [[ -f $CONTAINERS/test1/settings.sh ]]
'

test_case 'cs init (already exists)' '
    [[ -f $CONTAINERS/test1/settings.sh ]] &&
    cd $CONTAINERS/test1 &&
    cs init cs/test/app1 2>&1 | grep "already exists" &&
    cs init cs/test/app1 @test1 2>&1 | grep "already exists"
'

test_case 'cs init (wrong params)' '
    rm -rf $CONTAINERS/test1 &&
    mkdir -p $APPS/test1 &&
    cs init 2>&1 | grep "Usage:" &&
    cs init cs1 2>&1 | grep "Cannot find the directory of" &&
    cs init cs/test/app1 test11 2>&1 | grep "Usage:" &&
    cs init cs/test @test1 2>&1 | grep "There is no file"
'
