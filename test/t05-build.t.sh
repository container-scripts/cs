description 'Test cs build'

test_case 'cs init cs/test/app1 @test1' '
    cs pull cs &&
    rm -rf $CONTAINERS/test1 &&
    cs init cs/test/app1 @test1
'

test_case 'cs @test1 build' '
    cs @test1 build &&
    tail $CONTAINERS/test1/logs/cs-test-app1-*.out | grep "Successfully built" &&
    podman images --format "{{.Repository}}" | grep cs-test-app1
'

test_case 'cs build' '
    cd $CONTAINERS/test1 &&
    cs build &&
    tail logs/cs-test-app1-*.out | grep "Successfully built"
'
