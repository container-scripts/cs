description 'Test create'

test_case 'cs init cs/test/app1 @test1' '
    cs pull cs &&
    rm -rf $CONTAINERS/test1 &&
    cs init cs/test/app1 @test1
'

test_case 'cs @test1 build' '
    cs @test1 build &&
    podman images --format "{{.Repository}}" | grep cs-test-app1
'

test_case 'cs @test1 create' '
    cs @test1 create &&
    podman ps -a --format "{{.Names}}" | grep cs-test-app1
'
