PREFIX ?= /usr/local
DESTDIR ?=
BINDIR ?= $(DESTDIR)$(PREFIX)/bin
LIBDIR ?= $(DESTDIR)$(PREFIX)/lib
MANDIR ?= $(DESTDIR)$(PREFIX)/share/man/man1
BASHCOMP_PATH ?= $(DESTDIR)$(PREFIX)/share/bash-completion/completions

all: man install

install: uninstall
	install -v -d "$(LIBDIR)"/cs/
	cp -v -r sh/* "$(LIBDIR)"/cs/

	install -v -d "$(BINDIR)"/
	mv -v "$(LIBDIR)"/cs/cs.sh "$(BINDIR)"/cs
	sed -i "$(BINDIR)"/cs -e "s#LIBDIR=.*#LIBDIR=\"$(LIBDIR)/cs\"#"
	chmod -c 0755 "$(BINDIR)"/cs

	mv -v "$(LIBDIR)"/ds/playscript.sh "$(BINDIR)"/playscript
	chmod -c 0755 "$(BINDIR)"/playscript

	install -v -d "$(BASHCOMP_PATH)"
	mv -v "$(LIBDIR)"/cs/bash-completion.sh "$(BASHCOMP_PATH)"/cs
	chmod -c 0644 "$(BASHCOMP_PATH)"/cs

	install -v -d "$(MANDIR)/"
	install -v -m 0644 man/cs.1 "$(MANDIR)/cs.1"

uninstall:
	rm -vrf "$(BINDIR)"/cs "$(BINDIR)"/playscript "$(LIBDIR)/cs" "$(MANDIR)/cs.1" "$(BASHCOMP_PATH)"/cs

man: man/cs.1 man/cs.1.html

man/cs.1 man/cs.1.html: man/cs.1.ronn
	@man/make.sh

test:
	cd test/ && cs test

clean:
	rm -f man/cs.1 man/cs.1.html
	-for app in cs-test-app1 cs-test-app2 cs-test; do \
	    podman stop $$app 2>/dev/null ;\
	    podman rm   $$app 2>/dev/null ;\
	    podman rmi  $$app 2>/dev/null ;\
	done
	rm -rf test/cs-test
	rm -rf test/test-results
	rm -rf test/trash\ directory.*

.PHONY: install uninstall test clean
