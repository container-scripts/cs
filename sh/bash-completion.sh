# completion file for bash
# for help see:
#  - http://tldp.org/LDP/abs/html/tabexpansion.html
#  - https://www.debian-administration.org/article/317/An_introduction_to_bash_completion_part_2
#  - https://www.gnu.org/software/bash/manual/html_node/Programmable-Completion-Builtins.html

_cs()
{
    COMPREPLY=()   # Array variable storing the possible completions.
    local cur=${COMP_WORDS[COMP_CWORD]}     ## $2
    local prev=${COMP_WORDS[COMP_CWORD-1]}  ## $3
    local preprev=${COMP_WORDS[COMP_CWORD-2]}

    [[ "$preprev" == '@' ]] && _cs_commands $cur && return

    case $prev in
        cs)
            case $cur in
                -*) COMPREPLY=( $(compgen -W "-x -v --version -h --help" -- $cur) ) ;;
                @)  _cs_containers $cur ;;
                *)  _cs_commands $cur ;;
            esac
            ;;
        -x)
            case $cur in
                @)  _cs_containers $cur ;;
                *)  _cs_commands $cur ;;
            esac
            ;;
        @)
            _cs_containers "@$cur"
            ;;
        init)
            _cs_cmd_init $cur
            ;;
        inject)
            _cs_cmd_inject $cur
            ;;
        runtest|test)
            COMPREPLY=( $(compgen -f -X '!*.t.sh' -- $cur) )
            ;;
        *)
            _cs_custom_completion "$prev" "$cur"
            ;;
    esac
}

_cs_commands() {
    local commands="version start stop restart shell exec remove"

    local cmdlist=''
    for dir in \
        /usr/lib/cs \
        $(_cs_app_dir) \
        ${CSDIR:-$HOME/.cs} \
        $(_cs_container_dir)
    do
        [[ -d $dir/cmd/ ]] && cmdlist+=" $(ls $dir/cmd/)"
    done

    commands+=" ${cmdlist//.sh/}"
    COMPREPLY=( $(compgen -W "$commands" -- $1) )
}

_cs_containers() {
    local cur=$1
    local containers=''
    if [[ "${cur:1:1}" == '/' || "${cur:1:2}" == './' ]]; then
        compopt -o nospace
        containers=$(compgen -o dirnames -- ${cur:1})
    else
        local CONTAINERS=$(_cs_get_var CONTAINERS ${CSDIR:-$HOME/.cs}/config.sh)
        [[ -n $CONTAINERS ]] || return
        containers=$(ls $CONTAINERS)
    fi
    containers=$(echo $containers | sed -e 's/ / @/g')
    [[ -n "$containers" ]] && containers="@$containers"
    COMPREPLY=( $(compgen -W "$containers" -- $cur) )
}

_cs_cmd_init() {
    #compopt -o plusdirs
    local APPS=$(_cs_get_var APPS ${CSDIR:-$HOME/.cs}/config.sh)
    [[ -n $APPS ]] || return
    COMPREPLY=( $(compgen -W "$(ls $APPS)" -- $1) )
}

_cs_cmd_inject() {
    local scripts=''
    for dir in \
        /usr/lib/cs \
        $(_cs_app_dir) \
        ${CSDIR:-$HOME/.cs} \
        $(_cs_container_dir)
    do
        [[ -d $dir/scripts/ ]] && scripts+=" $(ls $dir/scripts/)"
    done

    COMPREPLY=( $(compgen -W "$scripts" -- $1) )
}

_cs_custom_completion() {
    # check that the function with the given name exists
    _cs_function_exists() {
        declare -Ff "$1" >/dev/null
        return $?
    }

    local prev=$1
    local cur=$2
    local cmd=$prev

    local file=$(_cs_app_dir)/bash-completion.sh
    [[ -f $file ]] && source $file || return
    _cs_function_exists "_cs_$cmd" && _cs_$cmd "$cur" "$prev"
}

# --------------------------------------------

_cs_get_var() {
    local var=$1
    local file=$2
    [[ -f $file ]] || return
    cat $file | grep "${var}=" | sed -e "s/${var}=//" | tr -d "'"'"'' '
}

_cs_app_dir() {
    local container_dir=$(_cs_container_dir)
    local APP=$(_cs_get_var APP $container_dir/settings.sh)
    [[ "${APP:0:1}" == '/' ]]  && echo "$APP" && return
    [[ -d "$container_dir/$APP" ]] && echo "$container_dir/$APP" && return
    local APPS=$(_cs_get_var APPS ${CSDIR:-$HOME/.cs}/config.sh)
    [[ -d "$APPS/$APP" ]] && echo "$APPS/$APP" && return
}

_cs_container_dir() {
    local CONTAINERS=$(_cs_get_var CONTAINERS ${CSDIR:-$HOME/.cs}/config.sh)
    local dir='.'
    if [[ "${COMP_WORDS[1]}" == '@' && $COMP_CWORD -gt 1 ]]; then
        dir="$CONTAINERS/${COMP_WORDS[2]}"
        [[ -d "$dir" ]] || dir="${COMP_WORDS[2]}"
    fi
    if [[ "${COMP_WORDS[2]}" == '@' && $COMP_CWORD -gt 2 ]]; then
        dir="$CONTAINERS/${COMP_WORDS[3]}"
        [[ -d "$dir" ]] || dir="${COMP_WORDS[3]}"
    fi
    echo "$dir"
}

# --------------------------------------------

complete -F _cs cs
