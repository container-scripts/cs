cmd_test_help() {
    cat <<_EOF
    test [-d|--debug] [<test-script.t.sh>...]
        Run the given test scripts inside the cs-test container.
        It actually call the command 'runtest' inside the container
        with the same options and arguments.

_EOF
}

cmd_test() {
    cs pull cs
    cs init cs/test/app @cs-test

    cs @cs-test build
    cs @cs-test create $(pwd)
    cs @cs-test config

    cs @cs-test inject install-cs.sh $APPS/cs
    cs @cs-test exec cs runtest "$@"
    cs @cs-test stop
}
