cmd_build_help() {
    cat <<_EOF
    build
        Build the podman image.

_EOF
}

cmd_build() {
    # copy podman files to a tmp dir
    # and preprocess Dockerfile
    local tmp=$(mktemp -u)
    cp -aT $APP_DIR $tmp
    m4 -I $APP_DIR -I "$LIBDIR/buildfiles" \
       $APP_DIR/Dockerfile > $tmp/Dockerfile

    # build the image
    log podman build "$@" --tag=$IMAGE $tmp/

    # clean up
    rm -rf $tmp/
}
