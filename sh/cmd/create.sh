cmd_create_help() {
    cat <<_EOF
    create
        Create the container.

_EOF
}

cmd_create() {
    # create a podman network if it does not yet exist
    local subnet=''
    [[ -n $SUBNET ]] && subnet="--subnet $SUBNET"
    podman network create $subnet $NETWORK 2>/dev/null

    # remove the container if it exists
    cmd_stop
    podman network disconnect $NETWORK $CONTAINER 2>/dev/null
    podman rm $CONTAINER 2>/dev/null

    # create a new container
    podman create --name=$CONTAINER --hostname=$CONTAINER \
        --restart=unless-stopped \
        --mount type=bind,source=$(pwd),destination=/host \
        $(_published_ports) \
        $(_network_and_aliases) \
        "$@" $IMAGE

    # add DOMAIN to wsproxy
    if [[ -n $DOMAIN ]]; then
        cs wsproxy add
        cs wsproxy ssl-cert
    fi
}

### published ports
_published_ports() {
    [[ -n $PORTS ]] || return

    local ports=''
    for port in $PORTS; do
        ports+=" --publish $port"
    done

    echo "$ports"
}

### network and aliases
_network_and_aliases() {
    [[ -n $NETWORK ]] || return

    local network=" --network $NETWORK"
    network+=" --network-alias $CONTAINER"
    if [[ -n $DOMAIN ]]; then
        for domain in $DOMAIN $DOMAINS; do
            network+=" --network-alias $domain"
        done
    fi

    echo "$network"
}
